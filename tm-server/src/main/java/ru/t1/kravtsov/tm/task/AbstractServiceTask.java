package ru.t1.kravtsov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.component.Server;

public abstract class AbstractServiceTask implements Runnable {

    @NotNull
    protected Server server;

    public AbstractServiceTask(@NotNull final Server server) {
        this.server = server;
    }

}
