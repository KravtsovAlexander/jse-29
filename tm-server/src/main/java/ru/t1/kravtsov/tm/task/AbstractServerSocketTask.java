package ru.t1.kravtsov.tm.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.component.Server;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServiceTask {

    @NotNull
    protected Socket socket;

    @Nullable
    protected String userId = null;

    public AbstractServerSocketTask(final @NotNull Server server, @NotNull final Socket socket) {
        super(server);
        this.socket = socket;
    }

    public AbstractServerSocketTask(
            final @NotNull Server server,
            @NotNull final Socket socket,
            @Nullable final String userId
    ) {
        super(server);
        this.socket = socket;
        this.userId = userId;
    }

}
