package ru.t1.kravtsov.tm.api.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.Socket;

public interface IEndpointClient {

    @Nullable
    Socket connect() throws IOException;

    @Nullable
    Socket disconnect() throws IOException;

    @Nullable
    Socket getSocket();

    void setSocket(@NotNull Socket socket);

}
