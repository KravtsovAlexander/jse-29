package ru.t1.kravtsov.tm.api.client;

import ru.t1.kravtsov.tm.api.endpoint.IDomainEndpoint;

public interface IDomainEndpointClient extends IDomainEndpoint, IEndpointClient {

}
