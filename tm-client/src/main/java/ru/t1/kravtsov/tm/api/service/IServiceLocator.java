package ru.t1.kravtsov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthEndpointClient getAuthEndpoint();

    @NotNull
    IDomainEndpointClient getDomainEndpoint();

    @NotNull
    IProjectEndpointClient getProjectEndpoint();

    @NotNull
    ITaskEndpointClient getTaskEndpoint();

    @NotNull
    IUserEndpointClient getUserEndpoint();

    @NotNull
    IEndpointClient getConnectionEndpoint();

}
