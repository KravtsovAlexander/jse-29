package ru.t1.kravtsov.tm.api.client;

import ru.t1.kravtsov.tm.api.endpoint.IAuthEndpoint;

public interface IAuthEndpointClient extends IAuthEndpoint, IEndpointClient {

}
