package ru.t1.kravtsov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.dto.request.TaskRemoveByNameRequest;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove task by name.";

    @NotNull
    public static final String NAME = "task-remove-by-name";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        getTaskEndpoint().removeTaskByName(new TaskRemoveByNameRequest(name));
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
