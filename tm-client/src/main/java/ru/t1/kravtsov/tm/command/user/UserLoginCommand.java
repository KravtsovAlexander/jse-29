package ru.t1.kravtsov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.dto.request.UserLoginRequest;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Login.";

    @NotNull
    public static final String NAME = "login";

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        getAuthEndpoint().login(new UserLoginRequest(login, password));
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
