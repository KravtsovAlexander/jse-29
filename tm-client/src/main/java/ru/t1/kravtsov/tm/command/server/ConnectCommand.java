package ru.t1.kravtsov.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.client.IEndpointClient;
import ru.t1.kravtsov.tm.command.AbstractCommand;
import ru.t1.kravtsov.tm.enumerated.Role;

import java.net.Socket;

public final class ConnectCommand extends AbstractCommand {

    @NotNull
    public static final String DESCRIPTION = "Connect to server";

    @NotNull
    public static final String NAME = "connect";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final IEndpointClient connectionEndpoint = getServiceLocator().getConnectionEndpoint();
        @Nullable final Socket socket = connectionEndpoint.connect();
        getServiceLocator().getAuthEndpoint().setSocket(socket);
        getServiceLocator().getProjectEndpoint().setSocket(socket);
        getServiceLocator().getDomainEndpoint().setSocket(socket);
        getServiceLocator().getUserEndpoint().setSocket(socket);
        getServiceLocator().getTaskEndpoint().setSocket(socket);
        getServiceLocator().getProjectEndpoint().setSocket(socket);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
