package ru.t1.kravtsov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.client.IAuthEndpointClient;
import ru.t1.kravtsov.tm.dto.request.UserLoginRequest;
import ru.t1.kravtsov.tm.dto.request.UserLogoutRequest;
import ru.t1.kravtsov.tm.dto.request.UserViewProfileRequest;
import ru.t1.kravtsov.tm.dto.response.UserLoginResponse;
import ru.t1.kravtsov.tm.dto.response.UserLogoutResponse;
import ru.t1.kravtsov.tm.dto.response.UserViewProfileResponse;

@NoArgsConstructor
public class AuthEndpointClient extends AbstractClient implements IAuthEndpointClient {

    public AuthEndpointClient(final @NotNull String host, final @NotNull Integer port) {
        super(host, port);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        @NotNull final UserLoginResponse response = call(request, UserLoginResponse.class);
        if (!response.getSuccess()) {
            throw new RuntimeException(response.getMessage());
        }
        return response;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserViewProfileResponse profile(@NotNull final UserViewProfileRequest request) {
        return call(request, UserViewProfileResponse.class);
    }

}
