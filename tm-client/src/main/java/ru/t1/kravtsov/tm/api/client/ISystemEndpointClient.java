package ru.t1.kravtsov.tm.api.client;

import ru.t1.kravtsov.tm.api.endpoint.ISystemEndpoint;

public interface ISystemEndpointClient extends ISystemEndpoint, IEndpointClient {

}
