package ru.t1.kravtsov.tm.api.client;

import ru.t1.kravtsov.tm.api.endpoint.ITaskEndpoint;

public interface ITaskEndpointClient extends ITaskEndpoint, IEndpointClient {

}
