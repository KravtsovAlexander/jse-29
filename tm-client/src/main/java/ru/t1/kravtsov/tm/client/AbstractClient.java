package ru.t1.kravtsov.tm.client;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.client.IEndpointClient;
import ru.t1.kravtsov.tm.dto.response.ApplicationErrorResponse;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
public abstract class AbstractClient implements IEndpointClient {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    @NotNull
    private Socket socket;

    public AbstractClient() {
    }

    public AbstractClient(@NotNull final String host, @NotNull final Integer port) {
        this.host = host;
        this.port = port;
    }

    public AbstractClient(@NotNull final AbstractClient client) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }

    @NotNull
    protected <T> T call(@NotNull final Object data, @NotNull final Class<T> clazz) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NotNull final ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    private OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    private InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    @Nullable
    public Socket connect() throws IOException {
        socket = new Socket(host, port);
        return socket;
    }

    @Nullable
    public Socket disconnect() throws IOException {
        socket.close();
        return socket;
    }

}
