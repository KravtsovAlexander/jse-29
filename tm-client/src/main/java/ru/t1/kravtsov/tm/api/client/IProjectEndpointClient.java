package ru.t1.kravtsov.tm.api.client;

import ru.t1.kravtsov.tm.api.endpoint.IProjectEndpoint;

public interface IProjectEndpointClient extends IProjectEndpoint, IEndpointClient {

}
