package ru.t1.kravtsov.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.client.IEndpointClient;
import ru.t1.kravtsov.tm.command.AbstractCommand;
import ru.t1.kravtsov.tm.enumerated.Role;

public class DisconnectCommand extends AbstractCommand {

    @NotNull
    public static final String DESCRIPTION = "Disconnect from server";

    @NotNull
    public static final String NAME = "disconnect";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final IEndpointClient connectionEndpoint = getServiceLocator().getConnectionEndpoint();
        connectionEndpoint.disconnect();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
