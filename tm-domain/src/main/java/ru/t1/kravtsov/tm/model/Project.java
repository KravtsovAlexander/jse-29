package ru.t1.kravtsov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.model.IWBS;
import ru.t1.kravtsov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    public Project(final @NotNull String name) {
        this.name = name;
    }

    public Project(final @NotNull String name, final @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public Project(final @NotNull String name, final @NotNull Status status) {
        this.name = name;
        this.status = status;
    }

}
