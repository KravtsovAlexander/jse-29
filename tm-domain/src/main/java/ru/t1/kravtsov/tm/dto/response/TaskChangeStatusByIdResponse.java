package ru.t1.kravtsov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.model.Task;

@NoArgsConstructor
public class TaskChangeStatusByIdResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIdResponse(final @Nullable Task task) {
        super(task);
    }

}
