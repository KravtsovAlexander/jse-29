package ru.t1.kravtsov.tm.exception.field;

public final class StatusEmptyException extends AbstractFieldException {

    public StatusEmptyException() {
        super("Error. Status is empty.");
    }

}
