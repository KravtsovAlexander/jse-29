package ru.t1.kravtsov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.model.User;

public class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final User user) {
        super(user);
    }

}
