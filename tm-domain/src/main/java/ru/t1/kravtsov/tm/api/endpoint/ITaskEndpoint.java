package ru.t1.kravtsov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.dto.request.*;
import ru.t1.kravtsov.tm.dto.response.*;

public interface ITaskEndpoint {

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskClearResponse clearTask(@NotNull TaskClearRequest request);

    @NotNull
    TaskCompleteByIdResponse completeTaskById(@NotNull TaskCompleteByIdRequest request);

    @NotNull
    TaskCompleteByIndexResponse completeTaskByIndex(@NotNull TaskCompleteByIndexRequest request);

    @NotNull
    TaskCreateResponse createTask(@NotNull TaskCreateRequest request);

    @NotNull
    TaskDisplayByIdResponse displayTaskById(@NotNull TaskDisplayByIdRequest request);

    @NotNull
    TaskDisplayByIndexResponse displayTaskByIndex(@NotNull TaskDisplayByIndexRequest request);

    @NotNull
    TaskListByProjectIdResponse listTaskByProjectId(@NotNull TaskListByProjectIdRequest request);

    @NotNull
    TaskListResponse listTask(@NotNull TaskListRequest request);

    @NotNull
    TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    TaskRemoveByNameResponse removeTaskByName(@NotNull TaskRemoveByNameRequest request);

    @NotNull
    TaskStartByIdResponse startTaskById(@NotNull TaskStartByIdRequest request);

    @NotNull
    TaskStartByIndexResponse startTaskByIndex(@NotNull TaskStartByIndexRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request);

    @NotNull
    TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request);

}
